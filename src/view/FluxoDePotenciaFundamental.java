package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.AcoesFluxoDePotenciaFundamental;
import model.CalculaFluxoDePotenciaFundamental;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.Color;

public class FluxoDePotenciaFundamental extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textFieldPotAtv;
	private JTextField textFieldPotRtv;
	private JTextField textFieldPotAp;
	private JTextField textFieldFatPot;
	private JTextField textFieldAngFaseTensao;
	private JTextField textFieldAmpTensao;
	private JTextField textFieldAngFaseCorrente;
	private JTextField textFieldAmpCorrente;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FluxoDePotenciaFundamental frame = new FluxoDePotenciaFundamental();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @return 
	 */
	public FluxoDePotenciaFundamental() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 997, 902);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		PainelGrafico panelTensao = new PainelGrafico(CalculaFluxoDePotenciaFundamental.scoresInit());
		panelTensao.setBounds(12, 51, 663, 250);
		contentPane.add(panelTensao);
		
		PainelGrafico panelCorrente = new PainelGrafico(CalculaFluxoDePotenciaFundamental.scoresInit());
		panelCorrente.setBounds(12, 327, 663, 250);
		contentPane.add(panelCorrente);
		
		PainelGrafico panelPotInst = new PainelGrafico(CalculaFluxoDePotenciaFundamental.scoresInit());
		panelPotInst.setBounds(12, 600, 663, 242);
		contentPane.add(panelPotInst);
		
		JLabel lblnguloDeFase = new JLabel("\u00C2ngulo de fase: [\u03B8\u00BA]");
		lblnguloDeFase.setBounds(692, 139, 132, 16);
		contentPane.add(lblnguloDeFase);
		
		JLabel lblEntradas = new JLabel("Entradas");
		lblEntradas.setFont(new Font("AgencyFB", Font.PLAIN, 25));
		lblEntradas.setBounds(451, 13, 107, 25);
		contentPane.add(lblEntradas);
		
		JLabel lblSadas = new JLabel("Sa\u00EDdas");
		lblSadas.setFont(new Font("AgencyFB", Font.PLAIN, 25));
		lblSadas.setBounds(456, 469, 90, 42);
		contentPane.add(lblSadas);
		
		JLabel lblnguloDeFase_1 = new JLabel("\u00C2ngulo de fase: [\u03B8\u00BA]");
		lblnguloDeFase_1.setBounds(692, 381, 132, 16);
		contentPane.add(lblnguloDeFase_1);
		
		JButton btnOkTensao = new JButton("Ok");
		btnOkTensao.setBackground(new Color(128, 128, 128));
		btnOkTensao.setBounds(854, 552, 97, 25);
		contentPane.add(btnOkTensao);
		
		JButton btnVoltar = new JButton("Voltar");
		btnVoltar.setBackground(new Color(128, 128, 128));
		btnVoltar.setBounds(854, 797, 97, 25);
		contentPane.add(btnVoltar);
		
		JLabel lblTensao = new JLabel("Tens\u00E3o");
		lblTensao.setBounds(306, 37, 56, 16);
		contentPane.add(lblTensao);
		
		JLabel lblCorrente = new JLabel("Corrente");
		lblCorrente.setBounds(306, 309, 56, 16);
		contentPane.add(lblCorrente);
		
		JLabel lblPotnciaInstantanea = new JLabel("Pot\u00EAncia instant\u00E2nea");
		lblPotnciaInstantanea.setBounds(282, 577, 132, 16);
		contentPane.add(lblPotnciaInstantanea);
		
		JLabel lblPotnciaAtiva_1 = new JLabel("Pot\u00EAncia Ativa");
		lblPotnciaAtiva_1.setBounds(701, 636, 132, 16);
		contentPane.add(lblPotnciaAtiva_1);
		
		JLabel lblPotnciaReativa = new JLabel("Pot\u00EAncia Reativa");
		lblPotnciaReativa.setBounds(701, 665, 132, 16);
		contentPane.add(lblPotnciaReativa);
		
		JLabel lblPotnciaAparente = new JLabel("Pot\u00EAncia Aparente");
		lblPotnciaAparente.setBounds(701, 694, 132, 16);
		contentPane.add(lblPotnciaAparente);
		
		JLabel lblFatorDe = new JLabel("Fator de Pot\u00EAncia");
		lblFatorDe.setBounds(701, 723, 132, 16);
		contentPane.add(lblFatorDe);
		
		textFieldPotAtv = new JTextField();
		textFieldPotAtv.setBounds(835, 633, 116, 22);
		contentPane.add(textFieldPotAtv);
		textFieldPotAtv.setColumns(10);
		
		textFieldPotRtv = new JTextField();
		textFieldPotRtv.setBounds(835, 662, 116, 22);
		contentPane.add(textFieldPotRtv);
		textFieldPotRtv.setColumns(10);
		
		textFieldPotAp = new JTextField();
		textFieldPotAp.setBounds(835, 691, 116, 22);
		contentPane.add(textFieldPotAp);
		textFieldPotAp.setColumns(10);
		
		textFieldFatPot = new JTextField();
		textFieldFatPot.setBounds(835, 720, 116, 22);
		contentPane.add(textFieldFatPot);
		textFieldFatPot.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Amplitude");
		lblNewLabel_1.setBounds(717, 203, 107, 16);
		contentPane.add(lblNewLabel_1);
		
		JLabel label = new JLabel("Amplitude");
		label.setBounds(717, 479, 76, 16);
		contentPane.add(label);
		
		textFieldAngFaseTensao = new JTextField();
		textFieldAngFaseTensao.setBounds(723, 168, 56, 22);
		contentPane.add(textFieldAngFaseTensao);
		textFieldAngFaseTensao.setColumns(10);
		
		textFieldAmpTensao = new JTextField();
		textFieldAmpTensao.setColumns(10);
		textFieldAmpTensao.setBounds(721, 232, 56, 22);
		contentPane.add(textFieldAmpTensao);
		
		textFieldAngFaseCorrente = new JTextField();
		textFieldAngFaseCorrente.setColumns(10);
		textFieldAngFaseCorrente.setBounds(723, 422, 56, 22);
		contentPane.add(textFieldAngFaseCorrente);
		
		textFieldAmpCorrente = new JTextField();
		textFieldAmpCorrente.setColumns(10);
		textFieldAmpCorrente.setBounds(727, 508, 56, 22);
		contentPane.add(textFieldAmpCorrente);
		
		btnVoltar.setActionCommand("Voltar");
		
		btnVoltar.addActionListener(new AcoesFluxoDePotenciaFundamental(this, panelTensao, panelCorrente, panelPotInst, 
				textFieldAmpTensao, textFieldAmpCorrente, textFieldAngFaseTensao, textFieldAngFaseCorrente, textFieldPotAtv, textFieldPotRtv, textFieldPotAp, textFieldFatPot));
		btnOkTensao.setActionCommand("Ok");
		
		btnOkTensao.addActionListener(new AcoesFluxoDePotenciaFundamental(this, panelTensao, panelCorrente, panelPotInst, 
				textFieldAmpTensao, textFieldAmpCorrente, textFieldAngFaseTensao, textFieldAngFaseCorrente, textFieldPotAtv, textFieldPotRtv, textFieldPotAp, textFieldFatPot));
				
	}
}

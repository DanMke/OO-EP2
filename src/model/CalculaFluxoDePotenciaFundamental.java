package model;

import java.util.ArrayList;
import java.util.List;

public class CalculaFluxoDePotenciaFundamental {

	public static List<Double> scoresInit() {
		List<Double> scores = new ArrayList<>();
		Double valor;
		
		for (double i=0; i<70; i=i+0.5) {
			valor = (double) 0;
			scores.add((double) valor);
		}
		
		return scores;
	}
	
	public static List<Double> scoresTensao(Double ampTensao, Double angulo) {
		List<Double> scores = new ArrayList<>();
		Double valor;
		
		for (double i = 0; i < 70; i= i + 0.5) {
			if(ampTensao < 0 || ampTensao > 220) {
				valor = (double) 0;
			}
			else if(angulo < -180 || angulo > 180) {
				valor = (double) 0;
			}
			else if(ampTensao == 0 && angulo == 0) {
				valor = (double) 0;
			}
			else {
				valor = ampTensao*Math.cos(Math.toRadians(2*Math.PI*60*i+angulo));
			}
			scores.add((double) valor);
		}
		
		return scores;
	}
	
	public static List<Double> scoresCorrente(Double ampCorrente, Double angulo) {
		List<Double> scores = new ArrayList<>();
		Double valor;
		for (double i=0; i<70; i=i+0.5) {
			if(ampCorrente<0 || ampCorrente>220) {
				valor = (double) 0;
			}
			else if(angulo < -180 || angulo > 180) {
				valor = (double) 0;
			}
			else if(ampCorrente==0 && angulo==0) {
				valor = (double) 0;
			}
			else {
				valor = ampCorrente*Math.cos(Math.toRadians(2*Math.PI*60*i+angulo));
			}
			scores.add((double) valor);
		}
		
		return scores;
	}
	
	public static List<Double> scoresResultado(List<Double> tensao, List<Double> corrente) {
		List<Double> scores = new ArrayList<>();
		
		Double valor;
		for (int i=0; i<70; i=i+1) {
			valor = tensao.get(i) * corrente.get(i);
			scores.add((double) valor);
		}
		
		return scores;	
	}
	
	public static int tensaoActived(Double ampCorrente, Double ampTensao, Double anguloTensao, Double anguloCorrente) {
		int resultado;
		
		resultado = (int) (ampCorrente * ampTensao * Math.cos(Math.toRadians(anguloTensao - anguloCorrente)));
		
		return resultado;
	}
	
	public static int tensaoReactived(Double ampCorrente, Double ampTensao, Double anguloTensao, Double anguloCorrente) {
		int resultado;
		
		resultado = (int) (ampCorrente * ampTensao * Math.sin(Math.toRadians(anguloTensao - anguloCorrente)));
		
		return resultado;
	}
	
	public static int tensaoApparent(Double ampCorrente, Double ampTensao) {
		int resultado;
		
		resultado = (int) (ampCorrente * ampTensao);
		
		return resultado;
	}
	
	public static double factorTensao(Double anguloTensao, Double anguloCorrente) {
		double valor = 0;
		
		if(anguloTensao == 0 && anguloCorrente == 0)
			return valor;
		
		if(anguloTensao == anguloCorrente)
			return (double) 1;
		
		valor = Math.cos(Math.toRadians(anguloTensao - anguloCorrente));
		
		return valor;
	}
}
package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import model.CalculaFluxoDePotenciaFundamental;
import view.PainelGrafico;
import view.PrimeiraTela;

public class AcoesFluxoDePotenciaFundamental implements ActionListener {
	
	JFrame telaPotFund;
	PainelGrafico graficoTensao;
	PainelGrafico graficoCorrente;
	PainelGrafico graficoResultado;
	JTextField txtAmp1;
	JTextField txtAmp2;
	JTextField txtAngTensao;
	JTextField txtAngCorrente;
	JTextField txtPotAtv;
	JTextField txtPotReat;
	JTextField txtPotApar;
	JTextField txtFatPot;
	
	public AcoesFluxoDePotenciaFundamental( JFrame telaPotFund, PainelGrafico graficoTensao, PainelGrafico graficoCorrente, PainelGrafico graficoResultado, 
											JTextField txtAmp1, JTextField txtAmp2, JTextField txtAngTensao, JTextField txtAngCorrente,
											JTextField txtPotAtv, JTextField txtPotReat, JTextField txtPotApar, JTextField txtFatPot ) {
		this.telaPotFund = telaPotFund;
		this.graficoTensao = graficoTensao;
		this.graficoCorrente = graficoCorrente;
		this.graficoResultado = graficoResultado;
		this.txtAmp1 = txtAmp1;
		this.txtAmp2 = txtAmp2;
		this.txtAngTensao = txtAngTensao;
		this.txtAngCorrente = txtAngCorrente;
		this.txtPotAtv = txtPotAtv;
		this.txtPotReat = txtPotReat;
		this.txtPotApar = txtPotApar;
		this.txtFatPot = txtFatPot;
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		String comando = e.getActionCommand();
		Double amp1, angTensao, amp2, angCorrente;
		
		amp1 = Double.parseDouble(txtAmp1.getText());
		angTensao = Double.parseDouble(txtAngTensao.getText());
		amp2 = Double.parseDouble(txtAmp2.getText());
		angCorrente = Double.parseDouble(txtAngCorrente.getText());
		
		if(comando.equals("Ok") && (amp1>220 || amp1<0)) {
			JOptionPane.showMessageDialog(telaPotFund,
			        "Escolha a amplitude da tens�o entre 0 e 220", "ERRO", JOptionPane.ERROR_MESSAGE);
		}
		
		if(comando.equals("Ok") && (amp2>220 || amp2<0)) {
			JOptionPane.showMessageDialog(telaPotFund,
			        "Escolha a amplitude da corrente entre 0 e 220", "ERRO", JOptionPane.ERROR_MESSAGE);
		}
		
		if(comando.equals("Ok") && ((angTensao>180 || angTensao<-180) || (angCorrente>180 || angCorrente<-180))) {
			JOptionPane.showMessageDialog(telaPotFund, "O angulo de fase deve estar entre -180 e 180 graus", "ERRO", JOptionPane.ERROR_MESSAGE);
		}
		
		else if(comando.equals("Ok") && comando.equals("Ok") && (amp1<=220 && amp1>=0) && (angCorrente<=180 && angCorrente>=-180) && (amp2<=220 && amp2>=0) && (angTensao<=180 && angTensao>=-180)){	
	
			List<Double> tensaoScores = new ArrayList<>();
			List<Double> correnteScores = new ArrayList<>();
			
			tensaoScores = CalculaFluxoDePotenciaFundamental.scoresTensao(amp1, angTensao);
			correnteScores = CalculaFluxoDePotenciaFundamental.scoresCorrente(amp2, angCorrente);
			txtPotAtv.setText(Integer.toString(CalculaFluxoDePotenciaFundamental.tensaoActived(amp2, amp1, angTensao, angCorrente)) + " WATT");
			txtPotReat.setText(Integer.toString(CalculaFluxoDePotenciaFundamental.tensaoReactived(amp2, amp1, angTensao, angCorrente)) + " VAR");
			txtPotApar.setText(Integer.toString(CalculaFluxoDePotenciaFundamental.tensaoApparent(amp2, amp1)) + " VAR");
			txtFatPot.setText(String.format("%.2f",CalculaFluxoDePotenciaFundamental.factorTensao(angTensao, angCorrente)));
			graficoTensao.setScores(tensaoScores);
			graficoCorrente.setScores(correnteScores);
			graficoResultado.setScores(CalculaFluxoDePotenciaFundamental.scoresResultado(tensaoScores, correnteScores));
		}
		
		if(comando.equals("Ok") && (amp1<=220 && amp1>=0) && (angCorrente<=180 && angCorrente>=-180) && (amp2<=220 && amp2>=0) && (angTensao<=180 && angTensao>=-180)) {
			List<Double> tensaoScores = new ArrayList<>();

			tensaoScores = CalculaFluxoDePotenciaFundamental.scoresTensao(amp1, angTensao);
			graficoTensao.setScores(tensaoScores);
		}
		
		if(comando.equals("Ok") && (amp1 <= 220 && amp1 >= 0) && (angCorrente <= 180 && angCorrente >= -180) && (amp2 <= 220 && amp2 >= 0) && (angTensao <= 180 && angTensao >= -180)) {
			List<Double> correnteScores = new ArrayList<>();

			correnteScores = CalculaFluxoDePotenciaFundamental.scoresCorrente(amp2, angTensao);
			graficoCorrente.setScores(correnteScores);
		}
		
		else if(comando.equals("Voltar")){
		    try {
		    	telaPotFund.dispose();
				JFrame telaInicial = new PrimeiraTela();
				telaInicial.setVisible(true);
		    } catch (Exception ex) {
		        Logger.getLogger(AcoesFluxoDePotenciaFundamental.class.getName()).log(Level.SEVERE, null, ex);
		    }
		    
		}  
		
	}
	
}

package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import view.FluxoDePotenciaFundamental;

public class AcoesPrimeiraTela implements ActionListener {

	
	JFrame telaInicial;
	
	public AcoesPrimeiraTela(JFrame telaInicial) {
		this.telaInicial = telaInicial;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		String comando = e.getActionCommand();
		
		if(comando.equals("TelaFluxoPotFund")){
			
		    try {
				telaInicial.dispose();
				JFrame potFund = new FluxoDePotenciaFundamental();
				potFund.setVisible(true);
		    } catch (Exception ex) {
		        Logger.getLogger(AcoesPrimeiraTela.class.getName()).log(Level.SEVERE, null, ex);
		    }

		} else if (comando.equals("Sair")) {
			
		    try {
				telaInicial.dispose();
		    } catch (Exception ex) {
		        Logger.getLogger(AcoesPrimeiraTela.class.getName()).log(Level.SEVERE, null, ex);
		    }
		}
	}


}

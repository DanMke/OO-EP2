# Exerc�cio Programa - APRENDER QEE - OO 2�/2017

-> O programa � uma calculadora com gr�ficos para uma disciplina do curso de Engenharia de Energia da FGA-UnB. O software foi feito em JAVA, utilizando a IDE Eclipse Oxygen.

-> O usu�rio do programa dever� instalar a IDE Eclipse Oxygen no site www.eclipse.org e clonar o reposit�rio presente no GitLab https://gitlab.com/DanMke/OO-EP2.git e abrir o programa na IDE Eclipse, ap�s isso o usu�rio iniciar� o programa.

-> O Software possui um MENU com duas op��es, a primeira op��o � para simular um fluxo de pot�ncia fundamental e outra � para sair do programa.

-> Quando o usu�rio escolher simular um fluxo de pot�ncia fundamental, ele dever� inserir uma amplitude e um �ngulo de fase para a tens�o, depois ir� inserir uma amplitude e um �ngulo de fase para a corrente e ir� gerar os gr�ficos de tens�o, corrente e de pot�ncia instant�nea e os c�lculos de pot�ncia ativa, reativa, aparente e o fator de pot�ncia.

-> O usu�rio dever� inserir amplitude entre 0 e 220 e �ngulo de fase entre -180 e 180 graus.
 
Autor: Daniel Maike Mendes Gon�alves - 16/0117003